<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
})->name('home');

Route::get('bvn','HomeController@bvn')->name('bvn');
Route::get('test','TestController@testEndpoint');

Route::post('verify/bank','BankController@verifyBank')->name('verify.bank');
Route::post('verify.bvn','BankController@verifyBvn')->name('verify.bvn');

