<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Scripts -->


    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">

    <!-- Styles -->
    <link href="{{ asset('css/bootstrap.min.css') }}" rel="stylesheet">
</head>
<body>
<div id="app">
    <nav class="navbar navbar-expand-md navbar-light bg-white shadow-sm">
        <div class="container">
            <a class="navbar-brand" href="{{ url('/') }}">
                {{ config('app.name', 'Laravel') }}
            </a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="{{ __('Toggle navigation') }}">
                <span class="navbar-toggler-icon"></span>
            </button>

            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <!-- Left Side Of Navbar -->
                <ul class="navbar-nav mr-auto">

                </ul>

            </div>
        </div>
    </nav>

    <main class="py-4">
        @yield('content')
    </main>
</div>
<script src="{{ asset('js/jquery-3.4.1.min.js') }}"></script>
<script src="{{ asset('js/bootstrap.min.js') }}" defer></script>

<script>
    function isBvnType(evt) {
        let k = evt.key;
        if(isNaN(k) ){
            return false;
        }else{
            return $('.bvn_field').val().length < 11?true:false;

        }

    }

    function isAccType(evt) {
        let k = evt.key;
        if(isNaN(k) ){
            return false;
        }else{
            return $('.acc_field').val().length < 10?true:false;
        }

    }

    function validate_acct() {
        let bankLoader = $('.load_acct');
        let acct_dom = $('.acct_detail_dom');
        acct_dom.children().remove();
        bankLoader.show();
        $.ajax({
            type: 'post',
            url: '{{ route('verify.bank') }}',
            data: {
                '_token': $('input[name=_token]').val(),
                'acct_num': $('.acc_field').val(),
                'bank': $('.bank_opt').val()
            },
            success: function(data) {
                console.log(data)
                if(data.status==='success'){
                    bankLoader.hide();
                    console.log(data)
                    acct_dom.append(
                        `<div class="card">
                            <div class="card-header">
                                Account Name
                            </div>
                            <div class="card-body">
                                <h5>${data.data.account_name}</h5>
                            </div>
                        </div>`);
                }
                bankLoader.hide();


            },

            error: function( req, status, err ) {
                bankLoader.hide();

                console.log( 'something went wrong', status,err, req);
            }
        });
    }

    function validate_bvn() {
        let bvnLoader = $('.load_bvn');
        let bvn_dom = $('.bvn_detail_dom');
        bvn_dom.children().remove();
        bvnLoader.show();

        $.ajax({
            type: 'post',
            url: '{{ route('verify.bvn') }}',
            data: {
                '_token': $('input[name=_token]').val(),
                'bvn_num': $('.bvn_field').val(),
            },
            success: function(data) {
                console.log(data)
                if(data.status==='success'){
                    bvnLoader.hide();
                    console.log(data)
                    bvn_dom.append(
                        `<div class="card">
                            <div class="card-header">
                                Details
                            </div>
                            <div class="card-body text-left">
                                <p class="mb-0">First Name</p>
                                <h5>${data.data.firstname}</h5>
                                <p class="mb-0">Last Name</p>
                                <h5>${data.data.lastname}</h5>
                                <p class="mb-0">Phone</p>
                                <h5>${data.data.phone}</h5>
                                <p class="mb-0">Date of Birth</p>
                                <h5>${data.data.dob}</h5>
                            </div>
                        </div>`);
                }else{
                    bvnLoader.hide();
                    console.log(data)
                    bvn_dom.append(
                        `<div class="card">
                            <div class="card-header">
                                Error
                            </div>
                            <div class="card-body text-left">
                                <p class="mb-0">${data.message}</p>
                            </div>
                        </div>`);
                }
                bvnLoader.hide();
            },

            error: function( req, status, err ) {
                bvnLoader.hide();
                console.log( 'something went wrong', status,err, req);
            }
        });
    }

    $(document).ready(function(){

    });
</script>
</body>
</html>
