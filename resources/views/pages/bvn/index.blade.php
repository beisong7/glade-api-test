@extends('layout.app')

@section('content')
    <div class="container">
        <h4 class="text-center mt-3">Start Verification</h4>
        <div class="row mt-5">
            <div class="col-md-6 mb-5">
                <div class="card">
                    <div class="card-header">
                        Input Bank Details
                    </div>
                    <div class="card-body">
                        <form action="#" onsubmit="event.preventDefault(); validate_acct()">
                            @csrf
                            <p class="text-muted">Required (Number Only)</p>
                            <input type="text" name="bvn" class="form-control acc_field" placeholder="Account Number" autocomplete="off" onkeypress="return isAccType(event)" autofocus required>
                            <br>
                            <select name="" class="form-control bank_opt" id="" required>
                                <option value="" selected disabled>Choose Bank</option>
                                @foreach($banks as $key=>$bank)
                                    <option value="{{ $key }}">{{ $bank }}</option>
                                @endforeach

                            </select>

                            <button type="submit" class="btn btn-dark btn-sm btn-block mt-3" >Verify Account Name</button>
                        </form>
                    </div>
                </div>
            </div>
            <div class="col-md-6 mb-5">
                <div class="card">
                    <div class="card-header">
                        Input BVN Details
                    </div>
                    <div class="card-body">
                        <form action="#" onsubmit="event.preventDefault(); validate_bvn()">
                            @csrf
                            <p class="text-muted">Required (Number Only)</p>
                            <input type="text" name="bvn" class="form-control bvn_field" placeholder="BVN Number" autocomplete="off" onkeypress="return isBvnType(event)" autofocus required>

                            <button type="submit" class="btn btn-dark btn-sm btn-block mt-3" >Load Account Details</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <br>
        <hr>

        <div class="row">
            <div class="col-md-6 text-center">
                <div class="load_acct" style="display: none">
                    <img src="{{ url('svg/spinner.svg') }}" alt="" style="width: 50px; height: 50px">
                </div>

                <div class="acct_detail_dom"></div>
            </div>

            <div class="col-md-6 text-center">
                <div class="load_bvn " style="display: none">
                    <img src="{{ url('svg/spinner.svg') }}" alt="" style="width: 50px; height: 50px">
                </div>

                <div class="bvn_detail_dom"></div>
            </div>
        </div>

    </div>

@endsection
