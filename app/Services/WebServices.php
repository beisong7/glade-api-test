<?php
/**
 * Created by PhpStorm.
 * User: hp
 * Date: 26/10/2020
 * Time: 5:55 AM
 */

namespace App\Services;


class WebServices
{
    public function request($post = [], string $resource, string $method = 'PUT'){
        $sample_headers = array(
            "key: Your Merchant Key",
            "mid: Your Merchant ID"
        );
        $headers = array(
            "key: yNBjvrTsxNWOoEeVtaRriEszmI1f1LAqVKa",
            "mid: GP_n3M6NC7AFi16BdyyDUNQgyCL0r0E3W34"
        );
        $static_url = env('GLADE','https://api.glade.ng/');
        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_URL => $static_url."$resource",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "$method",
            CURLOPT_POSTFIELDS => $post,
            CURLOPT_HTTPHEADER => $headers,
        ));
        $response = curl_exec($curl);
        $err = curl_error($curl);
        curl_close($curl);
        if ($err) {

            return false;
        } else {

            return $response;
        }

    }
}