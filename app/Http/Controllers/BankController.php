<?php

namespace App\Http\Controllers;
use App\Services\WebServices;
use Illuminate\Http\Request;

class BankController extends Controller
{
    public $services;

    public function __construct(WebServices $services)
    {
        $this->services = $services;
    }

    public function verifyBank(Request $request){

        $acct_num = $request->input('acct_num');
        $bank_code = $request->input('bank');

        $obj = [
            'inquire'=>'accountname', 'accountnumber'=>$acct_num, 'bankcode'=>$bank_code
        ];
        $res = $this->services->request(json_encode($obj), 'resources');

        return response()->json(json_decode($res), 200);
    }

    public function verifyBvn(Request $request){

        $bvn = $request->input('bvn_num');

        $obj = [
            'inquire'=>'bvn', 'bvn'=>$bvn
        ];
        $res = $this->services->request(json_encode($obj), 'resources');

        return response()->json(json_decode($res), 200);
    }
}
