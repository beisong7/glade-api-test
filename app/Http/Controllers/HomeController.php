<?php

namespace App\Http\Controllers;

use App\Services\WebServices;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    //

    public $services;

    public function __construct(WebServices $services)
    {
        $this->services = $services;
    }

    public function bvn(){
        //get bank list

        $banks = $this->services->request(json_encode(['inquire'=>'banks']), 'resources');

        $array = json_decode($banks, false);

//        return (array)$array;
//        return var_dump($array);
//        return $this->object_to_array($array);

        //todo - implement on functional endpoint
        return view('pages.bvn.index')->with(['banks'=>$array]);
    }

    public function object_to_array($data)
    {
        if (is_array($data) || is_object($data))
        {
            $result = array();
            foreach ($data as $key => $value)
            {
                $result[$key] = $this->object_to_array($value);
            }
            return $result;
        }
        return $data;
    }
}
