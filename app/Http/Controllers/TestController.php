<?php

namespace App\Http\Controllers;

use App\Services\WebServices;
use Illuminate\Http\Request;

class TestController extends Controller
{
    public $services;

    public function __construct(WebServices $services)
    {
        $this->services = $services;
    }

    //
    public function testEndpoint(){
        $json = ['inquire'=>'banks'];
        $res = $this->services->request(json_encode($json), 'resources');
        return response()->json(json_decode($res));
    }
}
